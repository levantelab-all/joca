require('dotenv').config()
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s ',
    title: 'Joca',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~/assets/css/components/dialog.css",
    "~/assets/css/components/breadcrumb.css",
    "~/assets/css/components/buttons.css",
    "~/assets/css/components/input-select.css",
    "~/assets/css/components/expansion-panel.css",

    "~/assets/css/typography.css",
    "~/assets/css/color.css",
    "~/assets/css/fonts-family.css",
    "~/assets/css/app.css",
    "~/assets/css/help-mobile.css",
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: "~/plugins/vue-code-highlight.js",
      ssr: false
    },
    {
      src: "~/plugins/v-mask.js",
      ssr: false
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: false,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    ['@nuxtjs/dotenv',{ filename: '.env' }],
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/dotenv',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/toast',
    // Simple usage
    'vuetify-dialog/nuxt',
    'vue-social-sharing/nuxt',
    '@nuxtjs/auth-next',
    'vue2-editor/nuxt'
  ],


  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.api_url + '' + process.env.versao_api,
  },

  auth: {
    strategies: {
      local: {
        user: {
          property: 'data'
        },
        endpoints: {
          login: { url: '/auth/login' },
          user: { url: '/auth/me' },
          logout: { url: '/auth/logout' },
        },
        tokenRequired: false,
        tokenType: false
      }
    },
    redirect: {
      home: '/painel-de-controle/geral',
    }
  },

  toast: {
    duration: 5000,
    position: 'top-center',
    register: [ // Register custom toasts
      {
        name: 'my-error',
        message: 'Oops...Something went wrong',
        options: {
          type: 'error'
        }
      }
    ]
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.BASE_URL,
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/scss/variables.scss'],
    treeShake: true,

    theme: {
      options: {
        customProperties: true
      },

      themes: {
        light: {
          // cores principais
          primary: '#004E34',
          primaryMid: '#14815D',
          primaryLight: '#45e18d',
          secondary: '#ff891c',
          secondaryMid: '#ff9f46',
          secondaryLight: '#ffc48e',
          accent: '#1cc4bc',
          accentMid: '#82ede8',
          accentLight: '#c9f7f5',
          //cores semânticas
          info: '#2196F3',
          infoLight: '#B7DAF6',
          warning: '#FFCF5C',
          warningLight: '#FFE29D',
          error: '#FF5E5E',
          errorLight: '#FDAFBB',
          success: '#21CF72',
          successLight: '#84EBB4',
          //estala de cinza
          grey1Joca: "#212529",
          grey2Joca: "#495057",
          grey3Joca: "#868e96",
          grey4Joca: "#ced4da",
          grey5Joca: "#e9ecef",
          grey6Joca: "#f8f9fa",
          //extras
          whiteJoca: "#FFF",
          green1Joca: "#e3fff4",
          green2Joca: "#60E6BA",
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
