import Vue from "vue";
import "vue-code-highlight/themes/duotone-sea.css";
import "vue-code-highlight/themes/window.css";
import VueCodeHighlight from 'vue-code-highlight';

Vue.use(VueCodeHighlight)
