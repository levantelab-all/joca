const typeUser = [
  {name:'professor',  description:'Professor(a) do INJC da UFRJ', img: 'file-books.svg', altImg:'Ilustração de três livros empilhados. Sendo da cor laranja, verde e azul.'},
  {name:'estudante', description:'Estudante do INJC da UFRJ', img: 'graduation-cap.svg', altImg:'Ilustração de um chapéu de formando preto com a corda laranja'},
  {name:'outros', description:'Tenho outro tipo de perfil', img: 'identification-people.svg', altImg:'Ilustração de identidade. Sendo ela azul e as informações laranja'},
]

export default typeUser;
