const typographyLato = [
  { name: 'h1', class: 'text-h1-joca lato-bold' },
  { name: 'h2', class: 'text-h2-joca lato-bold' },
  { name: 'h3', class: 'text-h3-joca lato-bold' },
  { name: 'h4', class: 'text-h4-joca lato-bold' },
  { name: 'h5', class: 'text-h5-joca lato-bold' },
  { name: 'h6', class: 'text-h6-joca lato-bold' },
  { name: 'body/lato', class: 'text-body-joca lato-bold' },
];

const typographyOpenSans = [
  { name: 'body-1', class: 'text-body-1-joca' },
  { name: 'body-2', class: 'text-body-2-joca' },
  { name: 'body-3', class: 'text-body-3-joca' },
  { name: 'body', class: 'text-body-joca' },
  { name: 'caption-1', class: 'text-caption-1-joca' },
  { name: 'caption-2', class: 'text-caption-2-joca' },
];

export default({
  typographyLato, typographyOpenSans
});
