const colorMain = [
  { name: 'primary' },
  { name: 'primaryMid' },
  { name: 'primaryLight' },
  { name: 'secondary' },
  { name: 'secondaryMid' },
  { name: 'secondaryLight' },
  { name: 'accent' },
  { name: 'accentMid' },
  { name: 'accentLight' },
  { name: 'green1Joca' },
  { name: 'bg-degrade-bg-joca' },
];

const colorSemantics = [
  { name: 'success' },
  { name: 'successLight' },
  { name: 'info' },
  { name: 'infoLight' },
  { name: 'warning' },
  { name: 'warningLight' },
  { name: 'error' },
  { name: 'errorLight' },
];

const colorGrey = [
  { name: 'grey1Joca' },
  { name: 'grey2Joca' },
  { name: 'grey3Joca' },
  { name: 'grey4Joca' },
  { name: 'grey5Joca' },
  { name: 'grey6Joca' },
  { name: 'whiteJoca' },
];



export default ({
  colorMain, colorSemantics, colorGrey
});
