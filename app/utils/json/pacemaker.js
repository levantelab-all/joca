const logos = [
  {
    src: 'logo-ufrj.svg',
    alt: 'Logo UFRJ',
    to: null,
    url: 'https://ufrj.br/',
  },
  {
    src: 'logo-ccs.svg',
    alt: 'Logo ccs UFRJ',
    to: null,
    url: 'https://ccs.ufrj.br/',
  },
  {
    src: 'logo-injc.svg',
    alt: 'Logo injc UFRJ',
    to: 'https://injc.ufrj.br/',
    url: 'https://injc.ufrj.br/',
  },
  {
    src: 'logo-joca.svg',
    alt: 'Logo joca',
    to: '/',
    url: null,
  },
  {
    src: 'logo-faperj.svg',
    alt: 'Logo faperj',
    to: null,
    url: 'http://www.faperj.br/',
  },
];

export default logos;

