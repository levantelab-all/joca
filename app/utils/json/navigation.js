const jocaNav = [
  { name: 'A Plataforma', to: 'a-plataforma' },
  { name: 'Josué de Castro', to: 'josue-de-castro' },
];

const helpNav = [
  { name: 'FAQ', to: 'perguntas-frequentes' },
  { name: 'Contato', to: 'contato' },
  { name: 'Acessibilidade', to: 'acessibilidade' },
];

const busyness = [
  {name : 'Termos de Uso', to:'termos-de-uso' },
  {name : 'Políticas de Privacidade', to:'politica-de-privacidade' },

]


export default({
  jocaNav,
  helpNav,
  busyness
});
