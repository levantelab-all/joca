export const state = () => ({
    areas: [
        {value: 'graduacao', text: 'Graduação'},
        {value: 'pos_graduacao', text: 'Pós-Graduação'},
        {value: 'outros', text: 'Outros'},
    ],
    recurso_tipos: [],
    licencas: [
        {value: '1', sigla: 'CC-BY', text: '(Esta licença permite que outros distribuam, remixem, adaptem e criem a partir do seu trabalho, mesmo para fins comerciais, desde que lhe atribuam o devido crédito pela criação original)', icon: '/images/licenca-de-uso/CC-BY.png'},
        {value: '2', sigla: 'CC-BY-SA', text: '(Esta licença permite que outros remixem, adaptem e criem a partir do seu trabalho, mesmo para fins comerciais, desde que lhe atribuam o devido crédito e que licenciem as novas criações sob termos idênticos)', icon: '/images/licenca-de-uso/CC-BY-SA.png'},
        {value: '3', sigla: 'CC-BY-NC', text: '(Esta licença permite que outros remixem, adaptem e criem a partir do seu trabalho para fins não comerciais e, embora os novos trabalhos tenham de lhe atribuir o devido crédito e não possam ser usados para fins comerciais, os usuários não têm de licenciar esses trabalhos derivados sob os mesmos termos)', icon: '/images/licenca-de-uso/CC-BY-NC.png'},
        {value: '4', sigla: 'CC-BY-NC-SA', text: '(Esta licença permite que outros remixem, adaptem e criem a partir do seu trabalho para fins não comerciais, desde que atribuam o devido crédito e que licenciem as novas criações sob termos idênticos)', icon: '/images/licenca-de-uso/CC-BY-NC-SA.png'},
    ],
    idiomas: [
        {value: 'portugues', text: 'Português'},
        {value: 'ingles', text: 'Inglês'},
        {value: 'espanhol', text: 'Espanhol'},
        {value: 'outros', text: 'Outros'},
    ]
  })
  
  export const mutations = {
      addTiposRecurso(state, tipos){
          state.recurso_tipos = tipos
      }
  }
  
  export const getters = {
    getTiposRecurso: (state) => {
      return state.recurso_tipos
    }
  }