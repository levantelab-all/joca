<?php


Route::resource('documents','Documents\DocumentsController');
Route::middleware(['auth:sanctum'])->resource('documents_type','Documents\DocumentsTypeController');
Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin'], function () {
    
    Route::post('/auth/register', 'AuthApiController@register');
    Route::post('/auth/login', 'AuthApiController@login');
    Route::post('/auth/alter-password', 'AuthApiController@alterPassword');
    Route::post('/auth/verifica-link-recuperacao-senha', 'AuthApiController@verificaLinkRecuperacaoSenha');
    Route::post('/auth/confirmar-reset-senha', 'AuthApiController@confirmarResetSenha');
    
    Route::group(['prefix' => 'ferramentas'], function() {
        Route::get('/cursos', 'FerramentasController@cursos');
    });
    
    Route::group(['middleware' => ['auth:sanctum']], function() {
        // User autenticated
        Route::get('/auth/me', 'AuthApiController@me');
        Route::post('/auth/logout', 'AuthApiController@logout');

        // Permissions
        Route::apiResource('permissions', 'PermissionsApiController');
        
        // Roles
        Route::apiResource('roles', 'RolesApiController');
    
        // Users
        Route::post('users/alter-password', 'UsersApiController@alterPassword');
        Route::apiResource('users', 'UsersApiController');
    
        // Recursoss
        Route::apiResource('recursosses', 'RecursossApiController');
    
        // Trilha
        Route::apiResource('trilhas', 'TrilhaApiController');
    
        // Divugacoes
        Route::apiResource('divugacos', 'DivugacoesApiController');
    
        // Cometario
        Route::apiResource('comentarios', 'CometarioApiController');
    
        // Tipo De Recursos
        Route::apiResource('tipo-de-recursos', 'TipoDeRecursosApiController');
    
        // Tipo De Midia
        Route::apiResource('tipo-de-midia', 'TipoDeMidiaApiController');
    
        // Seguidores
        Route::apiResource('seguidores', 'SeguidoresApiController');
    
        // Save
        Route::apiResource('saves', 'SaveApiController');

        Route::apiResource('eixos-tematicos', 'EixoTematicoApiController');
    });
});
