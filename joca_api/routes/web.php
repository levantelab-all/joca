<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Recursoss
    Route::delete('recursosses/destroy', 'RecursossController@massDestroy')->name('recursosses.massDestroy');
    Route::resource('recursosses', 'RecursossController');

    // Trilha
    Route::delete('trilhas/destroy', 'TrilhaController@massDestroy')->name('trilhas.massDestroy');
    Route::resource('trilhas', 'TrilhaController');

    // Divugacoes
    Route::delete('divugacos/destroy', 'DivugacoesController@massDestroy')->name('divugacos.massDestroy');
    Route::resource('divugacos', 'DivugacoesController');

    // Cometario
    Route::delete('cometarios/destroy', 'CometarioController@massDestroy')->name('cometarios.massDestroy');
    Route::resource('cometarios', 'CometarioController');

    // Tipo De Recursos
    Route::delete('tipo-de-recursos/destroy', 'TipoDeRecursosController@massDestroy')->name('tipo-de-recursos.massDestroy');
    Route::resource('tipo-de-recursos', 'TipoDeRecursosController');

    // Tipo De Midia
    Route::delete('tipo-de-midia/destroy', 'TipoDeMidiaController@massDestroy')->name('tipo-de-midia.massDestroy');
    Route::resource('tipo-de-midia', 'TipoDeMidiaController');

    // Seguidores
    Route::delete('seguidores/destroy', 'SeguidoresController@massDestroy')->name('seguidores.massDestroy');
    Route::resource('seguidores', 'SeguidoresController');

    // Save
    Route::delete('saves/destroy', 'SaveController@massDestroy')->name('saves.massDestroy');
    Route::resource('saves', 'SaveController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
    // Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
