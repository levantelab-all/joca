echo "Dando permissão na pasta storage"
chmod 755 -R storage/
echo "Entrei na pasta laradock"
cd laradock

echo "Vou buildar o server docker-compose up -d --build php-worker nginx redis workspace"
# Instalando o docker com todos seus componentes
docker-compose up -d --build php-worker nginx redis workspace certbot
echo "PERMISSION PASTA STORAGE"
docker exec -it laradock_workspace_1  chmod 777 -R storage/
echo "PERMISSION PASTA bootstrap"
docker exec -it laradock_workspace_1  chmod 777 -R bootstrap/
echo "Se até aqui tudo certo agora vou rodar compose install --ignore-platform-reqs"
docker exec -it laradock_workspace_1  composer install --ignore-platform-reqs
echo "Se até aqui tudo certo agora vou rodar php artisan migrate"
docker exec -it laradock_workspace_1  php artisan migrate
echo "Se até aqui tudo certo agora vou rodar npm install"
docker exec -it laradock_workspace_1  npm install
echo "Se até aqui tudo certo agora vou rodar npm run dev -- BUILD FRONTEND DO BACKEND"
docker exec -it laradock_workspace_1  npm run dev
echo "FIM DO BUILD! :)"


#AGORA VAI 