<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecursossTipoDeRecursoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recursoss_tipo_de_recurso', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recursoss_id');
            $table->unsignedBigInteger('tipo_de_recurso_id');
            $table->timestamps();

            $table->foreign('recursoss_id')
            ->on('recursosses')
            ->references('id')
            ->onDelete('cascade');

            $table->foreign('tipo_de_recurso_id')
            ->on('tipo_de_recursos')
            ->references('id')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recursoss_tipo_de_recurso');
    }
}
