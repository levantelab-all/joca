<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToUsersTable extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('aproved_by_id')->nullable();
            $table->foreign('aproved_by_id', 'aproved_by_fk_5910690')->references('id')->on('users');
        });
    }
}
