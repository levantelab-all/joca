<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToSavesTable extends Migration
{
    public function up()
    {
        Schema::table('saves', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_fk_5920390')->references('id')->on('users');
            $table->unsignedBigInteger('recurso_id')->nullable();
            $table->foreign('recurso_id', 'recurso_fk_5920391')->references('id')->on('recursosses');
        });
    }
}
