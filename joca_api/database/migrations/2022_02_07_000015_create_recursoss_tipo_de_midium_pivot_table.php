<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecursossTipoDeMidiumPivotTable extends Migration
{
    public function up()
    {
        Schema::create('recursoss_tipo_de_midium', function (Blueprint $table) {
            $table->unsignedBigInteger('tipo_de_midium_id');
            $table->foreign('tipo_de_midium_id', 'tipo_de_midium_id_fk_5920360')->references('id')->on('tipo_de_midia')->onDelete('cascade');
            $table->unsignedBigInteger('recursoss_id');
            $table->foreign('recursoss_id', 'recursoss_id_fk_5920360')->references('id')->on('recursosses')->onDelete('cascade');
        });
    }
}
