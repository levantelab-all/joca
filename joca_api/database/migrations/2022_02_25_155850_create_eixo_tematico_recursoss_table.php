<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEixoTematicoRecursossTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eixo_tematico_recursoss', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recursoss_id');
            $table->unsignedBigInteger('eixo_tematico_id');

            $table->foreign('recursoss_id')
            ->on('recursosses')
            ->references('id')
            ->onDelete('cascade');

            $table->foreign('eixo_tematico_id')
            ->on('eixo_tematicos')
            ->references('id')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eixo_tematico_recursoss');
    }
}
