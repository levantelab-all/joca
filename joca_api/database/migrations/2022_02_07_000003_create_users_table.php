<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable()->unique();
            $table->datetime('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('remember_token')->nullable();
            $table->string('sobrenome')->nullable();
            $table->string('nome_social')->nullable();
            $table->string('genero')->nullable();
            $table->date('data_nascimento')->nullable();
            $table->string('user_type')->nullable();
            $table->integer('permissao')->nullable();
            $table->string('matricula')->nullable();
            $table->string('curso')->nullable();
            $table->string('cep')->nullable();
            $table->string('estado')->nullable();
            $table->string('municipio')->nullable();
            $table->string('area_atuacao')->nullable();
            $table->string('instituicao_de_atuacao')->nullable();
            $table->string('como_conheceu')->nullable();
            $table->boolean('termos_accept')->default(0)->nullable();
            $table->boolean('accept_emails')->default(0)->nullable();
            $table->string('numero_ceap')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
