<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecursossesTable extends Migration
{
    public function up()
    {
        Schema::create('recursosses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('uri')->unique();
            $table->string('descricao');
            $table->string('palavras_chave')->nullable();
            $table->string('autoria')->nullable();
            $table->string('co_autoria')->nullable();
            $table->string('licenca');
            $table->string('idioma')->nullable();
            $table->string('sugerido_para')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('removed_by')->nullable();
            $table->boolean('accepted_terms_ad')->default(0)->nullable();
            $table->boolean('accepted_terms_offensive')->default(0)->nullable();
            $table->boolean('accepted_terms_politics')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
