<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToSeguidoresTable extends Migration
{
    public function up()
    {
        Schema::table('seguidores', function (Blueprint $table) {
            $table->unsignedBigInteger('follow_id')->nullable();
            $table->foreign('follow_id', 'follow_fk_5920370')->references('id')->on('users');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_fk_5920371')->references('id')->on('users');
        });
    }
}
