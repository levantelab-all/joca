<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecursossTrilhaPivotTable extends Migration
{
    public function up()
    {
        Schema::create('recursoss_trilha', function (Blueprint $table) {
            $table->unsignedBigInteger('trilha_id');
            $table->foreign('trilha_id', 'trilha_id_fk_5910861')->references('id')->on('trilhas')->onDelete('cascade');
            $table->unsignedBigInteger('recursoss_id');
            $table->foreign('recursoss_id', 'recursoss_id_fk_5910861')->references('id')->on('recursosses')->onDelete('cascade');
        });
    }
}
