<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCometariosTable extends Migration
{
    public function up()
    {
        Schema::create('cometarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('comentario')->nullable();
            $table->integer('like')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
