<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoRecursoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('tipo_de_recursos')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        DB::table('tipo_de_recursos')->insert([
            ['name' => 'animacao', 'description' => 'Animação'],
            ['name' => 'aplicativo_movel', 'description' => 'Aplicativo móvel'],
            ['name' => 'apresentacao', 'description' => 'Apresentação'],
            ['name' => 'artigo', 'description' => 'Artigo'],
            ['name' => 'audio', 'description' => 'Áudio'],
            ['name' => 'estudio_dirigido', 'description' => 'Estudo dirigido'],
            ['name' => 'experimento_pratico', 'description' => 'Experimento prático'],
            ['name' => 'imagem', 'description' => 'Imagem'],
            ['name' => 'infografico', 'description' => 'Infográfico'],
            ['name' => 'jogo', 'description' => 'Jogo'],
            ['name' => 'livro_digital', 'description' => 'Livro digital'],
            ['name' => 'mapa', 'description' => 'Mapa'],
            ['name' => 'pdf', 'description' => 'PDF'],
            ['name' => 'plano_aula', 'description' => 'Plano de aula'],
            ['name' => 'power_point', 'description' => 'PowerPoint'],
            ['name' => 'simulador', 'description' => 'Simulador'],
            ['name' => 'software', 'description' => 'Software'],
            ['name' => 'texto', 'description' => 'Texto'],
            ['name' => 'video', 'description' => 'Vídeo'],
            ['name' => 'video_aula', 'description' => 'Videoaula'],
            ['name' => 'website_externo', 'description' => 'Website externo'],
            ['name' => 'outros', 'description' => 'Outros']
        ]);
    }
}
