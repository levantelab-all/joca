<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'                     => 1,
                'name'                   => 'Admin',
                'email'                  => 'admin@admin.com',
                'password'               => bcrypt('password'),
                'remember_token'         => null,
                'sobrenome'              => '',
                'nome_social'            => '',
                'genero'                 => '',
                'matricula'              => '',
                'curso'                  => '',
                'cep'                    => '',
                'estado'                 => '',
                'municipio'              => '',
                'area_atuacao'           => '',
                'instituicao_de_atuacao' => '',
                'como_conheceu'          => '',
                'numero_ceap'            => '',
            ],
        ];

        User::insert($users);
    }
}
