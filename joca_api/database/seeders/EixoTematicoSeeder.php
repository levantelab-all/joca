<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EixoTematicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('eixo_tematicos')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        DB::table('eixo_tematicos')->insert([
            ['titulo' => 'Saúde'],
            ['titulo' => 'Meio ambiente e Sustentabilidade'],
            ['titulo' => 'Alimentos e Culinária'],
            ['titulo' => 'Administração e Gestão'],
            ['titulo' => 'Outros'],
        ]);
    }
}
