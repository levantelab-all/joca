<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'recurso_access',
            ],
            [
                'id'    => 18,
                'title' => 'recursoss_create',
            ],
            [
                'id'    => 19,
                'title' => 'recursoss_edit',
            ],
            [
                'id'    => 20,
                'title' => 'recursoss_show',
            ],
            [
                'id'    => 21,
                'title' => 'recursoss_delete',
            ],
            [
                'id'    => 22,
                'title' => 'recursoss_access',
            ],
            [
                'id'    => 23,
                'title' => 'trilha_create',
            ],
            [
                'id'    => 24,
                'title' => 'trilha_edit',
            ],
            [
                'id'    => 25,
                'title' => 'trilha_show',
            ],
            [
                'id'    => 26,
                'title' => 'trilha_delete',
            ],
            [
                'id'    => 27,
                'title' => 'trilha_access',
            ],
            [
                'id'    => 28,
                'title' => 'divugaco_create',
            ],
            [
                'id'    => 29,
                'title' => 'divugaco_edit',
            ],
            [
                'id'    => 30,
                'title' => 'divugaco_show',
            ],
            [
                'id'    => 31,
                'title' => 'divugaco_delete',
            ],
            [
                'id'    => 32,
                'title' => 'divugaco_access',
            ],
            [
                'id'    => 33,
                'title' => 'divulgaco_access',
            ],
            [
                'id'    => 34,
                'title' => 'cometario_create',
            ],
            [
                'id'    => 35,
                'title' => 'cometario_edit',
            ],
            [
                'id'    => 36,
                'title' => 'cometario_show',
            ],
            [
                'id'    => 37,
                'title' => 'cometario_delete',
            ],
            [
                'id'    => 38,
                'title' => 'cometario_access',
            ],
            [
                'id'    => 39,
                'title' => 'tipo_de_recurso_create',
            ],
            [
                'id'    => 40,
                'title' => 'tipo_de_recurso_edit',
            ],
            [
                'id'    => 41,
                'title' => 'tipo_de_recurso_show',
            ],
            [
                'id'    => 42,
                'title' => 'tipo_de_recurso_delete',
            ],
            [
                'id'    => 43,
                'title' => 'tipo_de_recurso_access',
            ],
            [
                'id'    => 44,
                'title' => 'tipo_de_midium_create',
            ],
            [
                'id'    => 45,
                'title' => 'tipo_de_midium_edit',
            ],
            [
                'id'    => 46,
                'title' => 'tipo_de_midium_show',
            ],
            [
                'id'    => 47,
                'title' => 'tipo_de_midium_delete',
            ],
            [
                'id'    => 48,
                'title' => 'tipo_de_midium_access',
            ],
            [
                'id'    => 49,
                'title' => 'seguidore_create',
            ],
            [
                'id'    => 50,
                'title' => 'seguidore_edit',
            ],
            [
                'id'    => 51,
                'title' => 'seguidore_show',
            ],
            [
                'id'    => 52,
                'title' => 'seguidore_delete',
            ],
            [
                'id'    => 53,
                'title' => 'seguidore_access',
            ],
            [
                'id'    => 54,
                'title' => 'save_create',
            ],
            [
                'id'    => 55,
                'title' => 'save_edit',
            ],
            [
                'id'    => 56,
                'title' => 'save_show',
            ],
            [
                'id'    => 57,
                'title' => 'save_delete',
            ],
            [
                'id'    => 58,
                'title' => 'save_access',
            ],
            [
                'id'    => 59,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
