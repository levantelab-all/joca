<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecuperarSenha extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $user;
    private $token;
    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Alteração de Senha');
        $this->to($this->user->email, $this->user->name);
        $url = env('APP_URL_SITE').'/redefinir-senha/'.$this->token;
        return $this->markdown('mail.RecuperarSenha')->with([
            'name' => $this->user->name,
            'email' => $this->user->email,
            'url' => $url
        ]);
    }
}
