<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;

    static function getAll(){
        $cursos = [
            ['value' => 1, 'text' => 'Curso 1'],
            ['value' => 2, 'text' => 'Curso 2']
        ];

        return $cursos;
    }
}
