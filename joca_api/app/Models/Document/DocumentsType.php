<?php

namespace App\Models\Document;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
class DocumentsType extends Model
{
    use SoftDeletes;

    protected $table = 'documents_type';

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'table',
        'multiple',
    ];

    static $validate = [
        'table'=>'required|string|max:250',
        'multiple'=>'boolean'
    ];
}
