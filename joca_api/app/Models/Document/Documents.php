<?php

namespace App\Models\Document;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
   

    protected $table = 'documents';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'document_type_id',
        'url',
        'length',
        'external_id',
        'type',
        'name',
        'description',
        'extension',
        'mime_type',
        'thumb_url',
        'webvtt',
    ];

    public $search =[
        'url',
        'type',
        'name',
        'description',
    ];
    
    public $validate = [
        'document_type_id'=>'required',
        'url'=> 'string|required',
        'length'=>'required'
    ];
    
    public function documents_type(){
        return $this->hasOne('App\Models\Document\DocumentsType','id','document_type_id');
    }

}
