<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trilha extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'trilhas';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'titulo',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function recursos()
    {
        return $this->belongsToMany(Recursoss::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
