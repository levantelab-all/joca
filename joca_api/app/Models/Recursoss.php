<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Recursoss extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'recursosses';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function tiposRecurso()
    {
        return $this->belongsToMany(TipoDeRecurso::class);
    }

    protected $fillable = [
        'accepted_terms_ad',
        'accepted_terms_offensive',
        'accepted_terms_politics',
        'title',
        'descricao',
        'palavras_chave',
        'autoria',
        'co_autoria',
        'licenca',
        'idioma',
        'sugerido_para',
        'thumbnail',
        'links',
        'visibility',
        'user_id',
        'removed_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function recursosTrilhas()
    {
        return $this->belongsToMany(Trilha::class);
    }

    public function eixoTematicos()
    {
        return $this->belongsToMany(EixoTematico::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function setTitleAttribute($value){
        $this->attributes['uri'] = $value ? Str::slug($value) : null;
        $this->attributes['title'] = $value ? $value : null;
    }

    public function getLinksAttribute($value){
        return $value ? json_decode($value, true) : null;
    }

    public function setLinksAttribute($value){
        $this->attributes['links'] = $value ? json_encode($value) : null;
    }

    public function getSugeridoParaAttribute($value){
       return $value ? json_decode($value) : null;
    }

    public function getImageAttribute(){
        return (!empty($this->thumbnail) ? (Storage::disk('s3')->exists($this->thumbnail) ? Storage::url($this->thumbnail) : '') : '');
    }

    public function getCreatedAtAttribute($value){
        return (!empty($value) ? date('d/m/Y', strtotime($value)) : '');
    }
}
