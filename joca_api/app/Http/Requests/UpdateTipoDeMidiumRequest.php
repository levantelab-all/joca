<?php

namespace App\Http\Requests;

use App\Models\TipoDeMidium;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateTipoDeMidiumRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('tipo_de_midium_edit');
    }

    public function rules()
    {
        return [
            'recursos.*' => [
                'integer',
            ],
            'recursos' => [
                'array',
            ],
        ];
    }
}
