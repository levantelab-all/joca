<?php

namespace App\Http\Requests;

use App\Models\Save;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroySaveRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('save_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:saves,id',
        ];
    }
}
