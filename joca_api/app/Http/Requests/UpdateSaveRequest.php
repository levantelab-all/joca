<?php

namespace App\Http\Requests;

use App\Models\Save;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateSaveRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('save_edit');
    }

    public function rules()
    {
        return [];
    }
}
