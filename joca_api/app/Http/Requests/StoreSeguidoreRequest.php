<?php

namespace App\Http\Requests;

use App\Models\Seguidore;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreSeguidoreRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('seguidore_create');
    }

    public function rules()
    {
        return [];
    }
}
