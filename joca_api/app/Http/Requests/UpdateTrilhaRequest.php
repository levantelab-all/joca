<?php

namespace App\Http\Requests;

use App\Models\Trilha;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateTrilhaRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('trilha_edit');
    }

    public function rules()
    {
        return [
            'titulo' => [
                'string',
                'required',
            ],
            'status' => [
                'required',
            ],
            'recursos.*' => [
                'integer',
            ],
            'recursos' => [
                'array',
            ],
        ];
    }
}
