<?php

namespace App\Http\Requests;

use App\Models\Divugaco;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreDivugacoRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('divugaco_create');
    }

    public function rules()
    {
        return [
            'title' => [
                'string',
                'required',
            ],
        ];
    }
}
