<?php

namespace App\Http\Requests;

use App\Models\TipoDeRecurso;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreTipoDeRecursoRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('tipo_de_recurso_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
            'description' => [
                'string',
                'nullable',
            ],
        ];
    }
}
