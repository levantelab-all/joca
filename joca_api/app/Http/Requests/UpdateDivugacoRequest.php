<?php

namespace App\Http\Requests;

use App\Models\Divugaco;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateDivugacoRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('divugaco_edit');
    }

    public function rules()
    {
        return [
            'title' => [
                'string',
                'required',
            ],
        ];
    }
}
