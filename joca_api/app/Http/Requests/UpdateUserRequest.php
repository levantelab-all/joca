<?php

namespace App\Http\Requests;

use App\Models\User;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
            'email' => [
                'required',
                'unique:users,email,' . request()->route('user')->id,
            ],
            'roles.*' => [
                'integer',
            ],
            'roles' => [
                'required',
                'array',
            ],
            'sobrenome' => [
                'string',
                'required',
            ],
            'nome_social' => [
                'string',
                'nullable',
            ],
            'genero' => [
                'string',
                'required',
            ],
            'data_nascimento' => [
                'required',
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
            ],
            'user_type' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'permissao' => [
                'required',
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'matricula' => [
                'string',
                'nullable',
            ],
            'curso' => [
                'string',
                'required',
            ],
            'cep' => [
                'string',
                'required',
            ],
            'estado' => [
                'string',
                'required',
            ],
            'municipio' => [
                'string',
                'required',
            ],
            'area_atuacao' => [
                'string',
                'nullable',
            ],
            'instituicao_de_atuacao' => [
                'string',
                'nullable',
            ],
            'como_conheceu' => [
                'string',
                'nullable',
            ],
            'termos_accept' => [
                'required',
            ],
            'accept_emails' => [
                'required',
            ],
            'numero_ceap' => [
                'string',
                'nullable',
            ],
        ];
    }
}
