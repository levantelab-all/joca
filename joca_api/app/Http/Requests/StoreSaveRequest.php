<?php

namespace App\Http\Requests;

use App\Models\Save;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreSaveRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('save_create');
    }

    public function rules()
    {
        return [];
    }
}
