<?php

namespace App\Http\Requests;

use App\Models\Cometario;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateCometarioRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('cometario_edit');
    }

    public function rules()
    {
        return [
            'comentario' => [
                'string',
                'nullable',
            ],
            'like' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
