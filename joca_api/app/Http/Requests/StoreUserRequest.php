<?php

namespace App\Http\Requests;

use App\Models\User;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
            'email' => [
                'required',
                'unique:users',
            ],
            'password' => [
                'required',
                'confirmed'
            ],
            'sobrenome' => [
                'string',
                'required',
            ],
            'nome_social' => [
                'string',
                'nullable',
            ],
            'genero' => [
                'string',
                'required',
            ],
            'data_nascimento' => [
                'required',
            ],
            'user_type' => [
                'nullable',
            ],
            'permissao' => [
                'required',
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'matricula' => [
                'string',
                'nullable',
            ],
            'curso' => [
                'filled',
            ],
            'cep' => [
                'string',
                'required',
            ],
            'estado' => [
                'string',
                'required',
            ],
            'municipio' => [
                'string',
                'required',
            ],
            'area_atuacao' => [
                'string',
                'nullable',
            ],
            'instituicao_de_atuacao' => [
                'string',
                'nullable',
            ],
            'como_conheceu' => [
                'string',
                'nullable',
            ],
            'termos_accept' => [
                'required',
            ],
            'numero_ceap' => [
                'string',
                'nullable',
            ],
        ];
    }
}
