<?php

namespace App\Http\Requests;

use App\Models\Recursoss;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRecursossRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('recursoss_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:recursosses,id',
        ];
    }
}
