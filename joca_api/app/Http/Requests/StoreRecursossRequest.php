<?php

namespace App\Http\Requests;

use App\Models\Recursoss;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreRecursossRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('recursoss_create');
    }

    public function rules()
    {
        return [
            'title' => [
                'string',
                'required',
            ],
            'descricao' => [
                'string',
                'required',
            ],
            'palavras_chave' => [
                'string',
                'nullable',
            ],
            'autoria' => [
                'string',
                'nullable',
            ],
            'co_autoria' => [
                'string',
                'nullable',
            ],
            'licenca' => [
                'string',
                'required',
            ],
            'eixo_tematico' => [
                'string',
                'nullable',
            ],
            'idioma' => [
                'string',
                'nullable',
            ],
            'sugerido_para' => [
                'string',
                'nullable',
            ],
            'thumbnail' => [
                'string',
                'nullable',
            ],
            'removed_by' => [
                'string',
                'nullable',
            ],
        ];
    }
}
