<?php

namespace App\Http\Controllers\Documents;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Document\DocumentsType;
use App\Models\Document\Documents;
class DocumentsTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new DocumentsType();

        if ($request->has('filters.table')) {
            $query = $query->where('table', $request->filters['table']);
        }
        if ($request->has('filters.multiple')) {
            $query = $query->where('multiple', $request->filters['multiple']);
        }
        //Registro apagados
        if ($request->filled("filters.inative")) {
            $query = $query->onlyTrashed();
        }

        //Busca global todas as tabelas
        if ($request->filled("filters.pesquisar")) {
            $documents = $query::search($request->filters["pesquisar"])->paginate();
        } else {
            $documents = $query->get();
        }

        return $this->api_response($documents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = 400;
		$message = "Objeto Tipos de Marcas não recebido!";
		$query =  '';


		$query = new DocumentsType($request->all());

		$request->validate(DocumentsType::$validate);

		if ($query->save()) {
            $code = 201;
            $message = "Marca cadastrada com sucesso!";
        } else {
            $message = "Problema ao cadastrar Marca";
        }

		$query = ["documents_type" => $query];
		return $this->api_response($query, $code, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DocumentsType::find($id)->delete();
        return $this->api_response(['response' => true]);
    }
}
