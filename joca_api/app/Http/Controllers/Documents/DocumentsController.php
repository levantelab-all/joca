<?php

namespace App\Http\Controllers\Documents;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Document\Documents;
use App\Models\Document\DocumentsType;
use Illuminate\Support\Facades\Storage;
// use App\Models\User\User;
use Illuminate\Support\Str;
// use App\Libraries\ffmpeg\ffmpegLib;

class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Documents();

        if ($request->has('document_type_id')) {
            $query = $query->where('document_type_id', $request->document_type_id);
        }
        if ($request->has('external_id')) {
            $query = $query->where('external_id', $request->external_id);
        }
        if ($request->has('type')) {
            $query = $query->where('type', $request->type);
        }
        if ($request->has('extension')) {
            $query = $query->where('extension', $request->extension);
        }
        if ($request->has('mime_type')) {
            $query = $query->where('mime_type', $request->mime_type);
        }
        //Registro apagados
        if ($request->filled("inative")) {
            $query = $query->onlyTrashed();
        }
        $query = $query->orderBy('created_at', 'DESC');
        //Busca global todas as tabelas
        if ($request->filled("search")) {
            $documentos = new Documents();
            $documents = Documents::whereLike($documentos->search, $request->search)->paginate();
        } else {
            $documents = $query->paginate();
        }
        foreach ($documents as $key => $value) {
            $documents[$key]->document_type = DocumentsType::find($documents[$key]->document_type_id);
            if(isset($documents[$key]->document_type->table) && !empty($documents[$key]->document_type->table)){
                if($documents[$key]->document_type->table && $documents[$key]->external_id){
                    $documents[$key][$documents[$key]->document_type->table] = \DB::table($documents[$key]->document_type->table)->where('id', $documents[$key]->external_id)->first();
                }
            }
        }
        return response()->json($documents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $documento = $request->file();
        $docs = [];
        //verificando se o pedido é de imagem
        if(isset($documento->documents)){
            $documento = $documento->documents;
            unset($documento['documents']);
        }
        $time = time();
        foreach ($documento as $key => $doc) {
            
            $docs[$key] = $this->organizeDocument($doc);
            if ($request->document_type_id) {
                $docs[$key]['document_type_id'] = $request->document_type_id;
                $docstype = DocumentsType::find($request->document_type_id);
                
                $path = isset($docstype['table'])?$docstype['table']:null   .'/'. Str::slug($docstype['name']).(($request->external_id)? '/'. $request->external_id : ''). "/".$time;
            } else {
                $path = 'gerais';
                $docs[$key]['document_type_id'] = 0;
            }
            $docs[$key]['external_id'] = ($request->external_id) ? $request->external_id : null;
            $docs[$key]['url'] =  Storage::put($path, $doc, 'public');
            $docs[$key]['url'] = Storage::url($docs[$key]['url']);
            $docs[$key]['name'] = $request->name?$request->name:$docs[$key]['name'];
            $docs[$key]['description'] = $request->description?$request->description:'';
            
            $documents = new Documents($docs[$key]);
            $documents->save();
            // if($docs[$key]['type'] == 'video'){
            //     $documents['cloud_path'] = $path;
            
            //     $docs[$key] = ffmpegLib::editVideo($documents, $doc, $time);
            //     $docs[$key]['video_status']  = true;
            // }
            $docs[$key] = Documents::find($documents->id)->toArray();
        }
        return response()->json($docs);
    }
    public function uploaderInS3($data){

    }
    /**
     * organizando document.
     *
     * @param  string  $doc
     * @return array docs
     */
    public function organizeDocument($doc)
    {
        
        $response['name'] = $doc->getClientOriginalName();
        $response['mime_type'] = $doc->getMimeType();
        $response['extension'] = $doc->extension();
        // $name = $array[0] . md5(Carbon::now());
        $response['name'] = Str::slug($response['name'], '-');
        $response['type'] = explode('/', $response['mime_type'])[0];
        $response['length'] = $doc->getSize();
        //verificando se tem video para fazer a modificação necessária;
        
        return $response;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = "Success";
        $code = 201;
        $query = Documents::find($id);
        if(!$query->delete()){
            $code = 404;
            $message = 'Ouve um error ao deletar';
        }
        $this->api_response($query ,$code,$message);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getExternal(Request $request)
    {
        return $request;
        dd($request);
        return DB::table($request->table)->get();
    }
}
