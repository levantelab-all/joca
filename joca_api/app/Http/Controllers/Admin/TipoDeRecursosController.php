<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyTipoDeRecursoRequest;
use App\Http\Requests\StoreTipoDeRecursoRequest;
use App\Http\Requests\UpdateTipoDeRecursoRequest;
use App\Models\TipoDeRecurso;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TipoDeRecursosController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('tipo_de_recurso_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tipoDeRecursos = TipoDeRecurso::all();

        return view('admin.tipoDeRecursos.index', compact('tipoDeRecursos'));
    }

    public function create()
    {
        abort_if(Gate::denies('tipo_de_recurso_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tipoDeRecursos.create');
    }

    public function store(StoreTipoDeRecursoRequest $request)
    {
        $tipoDeRecurso = TipoDeRecurso::create($request->all());

        return redirect()->route('admin.tipo-de-recursos.index');
    }

    public function edit(TipoDeRecurso $tipoDeRecurso)
    {
        abort_if(Gate::denies('tipo_de_recurso_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tipoDeRecursos.edit', compact('tipoDeRecurso'));
    }

    public function update(UpdateTipoDeRecursoRequest $request, TipoDeRecurso $tipoDeRecurso)
    {
        $tipoDeRecurso->update($request->all());

        return redirect()->route('admin.tipo-de-recursos.index');
    }

    public function show(TipoDeRecurso $tipoDeRecurso)
    {
        abort_if(Gate::denies('tipo_de_recurso_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tipoDeRecursos.show', compact('tipoDeRecurso'));
    }

    public function destroy(TipoDeRecurso $tipoDeRecurso)
    {
        abort_if(Gate::denies('tipo_de_recurso_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tipoDeRecurso->delete();

        return back();
    }

    public function massDestroy(MassDestroyTipoDeRecursoRequest $request)
    {
        TipoDeRecurso::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
