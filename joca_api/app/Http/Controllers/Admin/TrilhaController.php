<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyTrilhaRequest;
use App\Http\Requests\StoreTrilhaRequest;
use App\Http\Requests\UpdateTrilhaRequest;
use App\Models\Recursoss;
use App\Models\Trilha;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TrilhaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('trilha_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $trilhas = Trilha::with(['recursos'])->get();

        return view('admin.trilhas.index', compact('trilhas'));
    }

    public function create()
    {
        abort_if(Gate::denies('trilha_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $recursos = Recursoss::pluck('title', 'id');

        return view('admin.trilhas.create', compact('recursos'));
    }

    public function store(StoreTrilhaRequest $request)
    {
        $trilha = Trilha::create($request->all());
        $trilha->recursos()->sync($request->input('recursos', []));

        return redirect()->route('admin.trilhas.index');
    }

    public function edit(Trilha $trilha)
    {
        abort_if(Gate::denies('trilha_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $recursos = Recursoss::pluck('title', 'id');

        $trilha->load('recursos');

        return view('admin.trilhas.edit', compact('recursos', 'trilha'));
    }

    public function update(UpdateTrilhaRequest $request, Trilha $trilha)
    {
        $trilha->update($request->all());
        $trilha->recursos()->sync($request->input('recursos', []));

        return redirect()->route('admin.trilhas.index');
    }

    public function show(Trilha $trilha)
    {
        abort_if(Gate::denies('trilha_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $trilha->load('recursos');

        return view('admin.trilhas.show', compact('trilha'));
    }

    public function destroy(Trilha $trilha)
    {
        abort_if(Gate::denies('trilha_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $trilha->delete();

        return back();
    }

    public function massDestroy(MassDestroyTrilhaRequest $request)
    {
        Trilha::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
