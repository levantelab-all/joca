<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySeguidoreRequest;
use App\Http\Requests\StoreSeguidoreRequest;
use App\Http\Requests\UpdateSeguidoreRequest;
use App\Models\Seguidore;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SeguidoresController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('seguidore_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $seguidores = Seguidore::with(['follow', 'user'])->get();

        return view('admin.seguidores.index', compact('seguidores'));
    }

    public function create()
    {
        abort_if(Gate::denies('seguidore_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $follows = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.seguidores.create', compact('follows', 'users'));
    }

    public function store(StoreSeguidoreRequest $request)
    {
        $seguidore = Seguidore::create($request->all());

        return redirect()->route('admin.seguidores.index');
    }

    public function edit(Seguidore $seguidore)
    {
        abort_if(Gate::denies('seguidore_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $follows = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $seguidore->load('follow', 'user');

        return view('admin.seguidores.edit', compact('follows', 'seguidore', 'users'));
    }

    public function update(UpdateSeguidoreRequest $request, Seguidore $seguidore)
    {
        $seguidore->update($request->all());

        return redirect()->route('admin.seguidores.index');
    }

    public function show(Seguidore $seguidore)
    {
        abort_if(Gate::denies('seguidore_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $seguidore->load('follow', 'user');

        return view('admin.seguidores.show', compact('seguidore'));
    }

    public function destroy(Seguidore $seguidore)
    {
        abort_if(Gate::denies('seguidore_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $seguidore->delete();

        return back();
    }

    public function massDestroy(MassDestroySeguidoreRequest $request)
    {
        Seguidore::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
