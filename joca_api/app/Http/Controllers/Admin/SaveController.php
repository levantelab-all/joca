<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySaveRequest;
use App\Http\Requests\StoreSaveRequest;
use App\Http\Requests\UpdateSaveRequest;
use App\Models\Recursoss;
use App\Models\Save;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SaveController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('save_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $saves = Save::with(['user', 'recurso'])->get();

        return view('admin.saves.index', compact('saves'));
    }

    public function create()
    {
        abort_if(Gate::denies('save_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $recursos = Recursoss::pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.saves.create', compact('recursos', 'users'));
    }

    public function store(StoreSaveRequest $request)
    {
        $save = Save::create($request->all());

        return redirect()->route('admin.saves.index');
    }

    public function edit(Save $save)
    {
        abort_if(Gate::denies('save_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $recursos = Recursoss::pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        $save->load('user', 'recurso');

        return view('admin.saves.edit', compact('recursos', 'save', 'users'));
    }

    public function update(UpdateSaveRequest $request, Save $save)
    {
        $save->update($request->all());

        return redirect()->route('admin.saves.index');
    }

    public function show(Save $save)
    {
        abort_if(Gate::denies('save_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $save->load('user', 'recurso');

        return view('admin.saves.show', compact('save'));
    }

    public function destroy(Save $save)
    {
        abort_if(Gate::denies('save_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $save->delete();

        return back();
    }

    public function massDestroy(MassDestroySaveRequest $request)
    {
        Save::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
