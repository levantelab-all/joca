<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCometarioRequest;
use App\Http\Requests\StoreCometarioRequest;
use App\Http\Requests\UpdateCometarioRequest;
use App\Models\Cometario;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CometarioController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('cometario_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cometarios = Cometario::all();

        return view('admin.cometarios.index', compact('cometarios'));
    }

    public function create()
    {
        abort_if(Gate::denies('cometario_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.cometarios.create');
    }

    public function store(StoreCometarioRequest $request)
    {
        $cometario = Cometario::create($request->all());

        return redirect()->route('admin.cometarios.index');
    }

    public function edit(Cometario $cometario)
    {
        abort_if(Gate::denies('cometario_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.cometarios.edit', compact('cometario'));
    }

    public function update(UpdateCometarioRequest $request, Cometario $cometario)
    {
        $cometario->update($request->all());

        return redirect()->route('admin.cometarios.index');
    }

    public function show(Cometario $cometario)
    {
        abort_if(Gate::denies('cometario_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.cometarios.show', compact('cometario'));
    }

    public function destroy(Cometario $cometario)
    {
        abort_if(Gate::denies('cometario_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cometario->delete();

        return back();
    }

    public function massDestroy(MassDestroyCometarioRequest $request)
    {
        Cometario::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
