<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyDivugacoRequest;
use App\Http\Requests\StoreDivugacoRequest;
use App\Http\Requests\UpdateDivugacoRequest;
use App\Models\Divugaco;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DivugacoesController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('divugaco_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $divugacos = Divugaco::with(['user'])->get();

        return view('admin.divugacos.index', compact('divugacos'));
    }

    public function create()
    {
        abort_if(Gate::denies('divugaco_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.divugacos.create', compact('users'));
    }

    public function store(StoreDivugacoRequest $request)
    {
        $divugaco = Divugaco::create($request->all());

        return redirect()->route('admin.divugacos.index');
    }

    public function edit(Divugaco $divugaco)
    {
        abort_if(Gate::denies('divugaco_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $divugaco->load('user');

        return view('admin.divugacos.edit', compact('divugaco', 'users'));
    }

    public function update(UpdateDivugacoRequest $request, Divugaco $divugaco)
    {
        $divugaco->update($request->all());

        return redirect()->route('admin.divugacos.index');
    }

    public function show(Divugaco $divugaco)
    {
        abort_if(Gate::denies('divugaco_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $divugaco->load('user');

        return view('admin.divugacos.show', compact('divugaco'));
    }

    public function destroy(Divugaco $divugaco)
    {
        abort_if(Gate::denies('divugaco_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $divugaco->delete();

        return back();
    }

    public function massDestroy(MassDestroyDivugacoRequest $request)
    {
        Divugaco::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
