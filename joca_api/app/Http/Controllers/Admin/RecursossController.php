<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRecursossRequest;
use App\Http\Requests\StoreRecursossRequest;
use App\Http\Requests\UpdateRecursossRequest;
use App\Models\Recursoss;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RecursossController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('recursoss_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $recursosses = Recursoss::all();

        return view('admin.recursosses.index', compact('recursosses'));
    }

    public function create()
    {
        abort_if(Gate::denies('recursoss_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.recursosses.create');
    }

    public function store(StoreRecursossRequest $request)
    {
        $recursoss = Recursoss::create($request->all());

        return redirect()->route('admin.recursosses.index');
    }

    public function edit(Recursoss $recursoss)
    {
        abort_if(Gate::denies('recursoss_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.recursosses.edit', compact('recursoss'));
    }

    public function update(UpdateRecursossRequest $request, Recursoss $recursoss)
    {
        $recursoss->update($request->all());

        return redirect()->route('admin.recursosses.index');
    }

    public function show(Recursoss $recursoss)
    {
        abort_if(Gate::denies('recursoss_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $recursoss->load('recursosTrilhas');

        return view('admin.recursosses.show', compact('recursoss'));
    }

    public function destroy(Recursoss $recursoss)
    {
        abort_if(Gate::denies('recursoss_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $recursoss->delete();

        return back();
    }

    public function massDestroy(MassDestroyRecursossRequest $request)
    {
        Recursoss::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
