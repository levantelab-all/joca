<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyTipoDeMidiumRequest;
use App\Http\Requests\StoreTipoDeMidiumRequest;
use App\Http\Requests\UpdateTipoDeMidiumRequest;
use App\Models\Recursoss;
use App\Models\TipoDeMidium;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TipoDeMidiaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('tipo_de_midium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tipoDeMidia = TipoDeMidium::with(['recursos'])->get();

        return view('admin.tipoDeMidia.index', compact('tipoDeMidia'));
    }

    public function create()
    {
        abort_if(Gate::denies('tipo_de_midium_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $recursos = Recursoss::pluck('title', 'id');

        return view('admin.tipoDeMidia.create', compact('recursos'));
    }

    public function store(StoreTipoDeMidiumRequest $request)
    {
        $tipoDeMidium = TipoDeMidium::create($request->all());
        $tipoDeMidium->recursos()->sync($request->input('recursos', []));

        return redirect()->route('admin.tipo-de-midia.index');
    }

    public function edit(TipoDeMidium $tipoDeMidium)
    {
        abort_if(Gate::denies('tipo_de_midium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $recursos = Recursoss::pluck('title', 'id');

        $tipoDeMidium->load('recursos');

        return view('admin.tipoDeMidia.edit', compact('recursos', 'tipoDeMidium'));
    }

    public function update(UpdateTipoDeMidiumRequest $request, TipoDeMidium $tipoDeMidium)
    {
        $tipoDeMidium->update($request->all());
        $tipoDeMidium->recursos()->sync($request->input('recursos', []));

        return redirect()->route('admin.tipo-de-midia.index');
    }

    public function show(TipoDeMidium $tipoDeMidium)
    {
        abort_if(Gate::denies('tipo_de_midium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tipoDeMidium->load('recursos');

        return view('admin.tipoDeMidia.show', compact('tipoDeMidium'));
    }

    public function destroy(TipoDeMidium $tipoDeMidium)
    {
        abort_if(Gate::denies('tipo_de_midium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tipoDeMidium->delete();

        return back();
    }

    public function massDestroy(MassDestroyTipoDeMidiumRequest $request)
    {
        TipoDeMidium::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
