<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\Admin\UserResource;
use App\Mail\RecuperarSenha;
use App\Models\PasswordReset;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class AuthApiController extends Controller
{
    public function register(StoreUserRequest $request){
        $user = User::register($request->all());
        if($user){
            $user->roles()->sync(2);
            return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
        }
    }

    public function login(Request $request){
        if(!auth()->attempt($request->only('email', 'password'))){
            return response()->json(['msg' => 'Email ou senha incorretos'], Response::HTTP_NOT_FOUND);
        }

        $token = auth()->user()->createToken('user-token');

        return [
            'message' => ['Logado com sucesso'],
            'token' => $token->plainTextToken
        ];
    }

    public function logout(){
        auth()->user()->tokens()->delete();
        return response()->json(['msg'=>'Successfully Logged out'], Response::HTTP_OK);
    }

    public function me(){
        $user = auth()->user();
        return new UserResource($user->load(['roles', 'aproved_by']));
    }

    public function alterPassword(Request $request){
        $user = User::where('email', $request->email)->first();
        if($user){
            $token = Str::random(60);
            $reset = PasswordReset::create([
                'email' => $request->email,
                'token' => $token
            ]);

            if($reset){
                Mail::send(new RecuperarSenha($user, $token));
                return response()->json([
                    'status' => true,
                    'msg' => 'Um email foi enviado para sua caixa de entrada, siga as orientações para alteração da senha!'
                 ],Response::HTTP_OK);
            }
        } else {
            return response()->json(['msg' => 'Usuário não encontrado, verifique o endereço de email!'], Response::HTTP_NOT_FOUND);
        }
    }

    public function verificaLinkRecuperacaoSenha(Request $request){
        $passwordReset = PasswordReset::where('token', $request->token)->first();

        if($passwordReset){
            if(Carbon::parse($passwordReset->created_at)->addMinutes(720)->isPast()){
                return response()->json(['msg' => 'Link para recuperação expirado! Favor tente novamente.'],Response::HTTP_NOT_FOUND);
            }else{
                return response()->json([
                    'data' => $passwordReset
                 ],Response::HTTP_OK);
            }
        }else{
            return response()->json(['msg' => 'Link para recuperação expirado! Favor tente novamente.'],Response::HTTP_NOT_FOUND);
        }
    }

    public function confirmarResetSenha(Request $request){
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);

        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();

        if(!$passwordReset){
            return response()->json(['msg' => 'Não foi possível encontrar pedido de recuperação de sua senha, o link pode ter expirado!'],Response::HTTP_NOT_FOUND);
        }

        $usuario = User::where('email', $passwordReset->email)->first();
        if (!$usuario){
            return response()->json(['msg' => 'Usuário não encontrado, favor, verifique seu email e tente novamente!'], Response::HTTP_NOT_FOUND);
        }

        $usuario->password = bcrypt($request->password);
        $usuario->save();

        return response()->json([
            'msg' => 'Senha atualizada com sucesso'
        ],Response::HTTP_OK);
    }

}
