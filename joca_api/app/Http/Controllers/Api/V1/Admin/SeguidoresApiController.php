<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSeguidoreRequest;
use App\Http\Requests\UpdateSeguidoreRequest;
use App\Http\Resources\Admin\SeguidoreResource;
use App\Models\Seguidore;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SeguidoresApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('seguidore_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SeguidoreResource(Seguidore::with(['follow', 'user'])->get());
    }

    public function store(StoreSeguidoreRequest $request)
    {
        $seguidore = Seguidore::create($request->all());

        return (new SeguidoreResource($seguidore))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Seguidore $seguidore)
    {
        abort_if(Gate::denies('seguidore_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SeguidoreResource($seguidore->load(['follow', 'user']));
    }

    public function update(UpdateSeguidoreRequest $request, $id)
    {
        $seguidore = Seguidore::find($id);
        $seguidore->update($request->all());

        return (new SeguidoreResource($seguidore))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Seguidore $seguidore)
    {
        abort_if(Gate::denies('seguidore_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $seguidore->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
