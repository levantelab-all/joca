<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSaveRequest;
use App\Http\Requests\UpdateSaveRequest;
use App\Http\Resources\Admin\SaveResource;
use App\Models\Save;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SaveApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('save_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SaveResource(Save::with(['user', 'recurso'])->get());
    }

    public function store(StoreSaveRequest $request)
    {
        $save = Save::create($request->all());

        return (new SaveResource($save))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Save $save)
    {
        abort_if(Gate::denies('save_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SaveResource($save->load(['user', 'recurso']));
    }

    public function update(UpdateSaveRequest $request, $id)
    {
        $save = Save::find($id);
        $save->update($request->all());

        return (new SaveResource($save))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Save $save)
    {
        abort_if(Gate::denies('save_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $save->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
