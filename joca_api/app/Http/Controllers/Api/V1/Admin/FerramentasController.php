<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Models\Curso;
use Illuminate\Http\Request;

class FerramentasController extends Controller
{
    public function cursos(){
        return Curso::getAll();
    }
}
