<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDivugacoRequest;
use App\Http\Requests\UpdateDivugacoRequest;
use App\Http\Resources\Admin\DivugacoResource;
use App\Models\Divugaco;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DivugacoesApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('divugaco_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DivugacoResource(Divugaco::with(['user'])->get());
    }

    public function store(StoreDivugacoRequest $request)
    {
        $divugaco = Divugaco::create($request->all());

        return (new DivugacoResource($divugaco))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Divugaco $divugaco)
    {
        abort_if(Gate::denies('divugaco_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DivugacoResource($divugaco->load(['user']));
    }

    public function update(UpdateDivugacoRequest $request, $id)
    {
        $divugaco = Divugaco::find($id);
        $divugaco->update($request->all());

        return (new DivugacoResource($divugaco))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Divugaco $divugaco)
    {
        abort_if(Gate::denies('divugaco_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $divugaco->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
