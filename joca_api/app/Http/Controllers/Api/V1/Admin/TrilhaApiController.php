<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTrilhaRequest;
use App\Http\Requests\UpdateTrilhaRequest;
use App\Http\Resources\Admin\TrilhaResource;
use App\Models\Trilha;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TrilhaApiController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('trilha_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $trilhas = new Trilha();
        if ($request->get('not_paginate')) {
            $trilhas = $trilhas->with(['recursos'])->get();
        }else{
            $trilhas = $trilhas->with(['recursos'])->paginate($request->get('show', 10));
        }

        return new TrilhaResource($trilhas);
    }

    public function store(StoreTrilhaRequest $request)
    {
        $trilha = Trilha::create($request->all());
        $trilha->recursos()->sync($request->input('recursos', []));

        return (new TrilhaResource($trilha))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Trilha $trilha)
    {
        abort_if(Gate::denies('trilha_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TrilhaResource($trilha->load(['recursos']));
    }

    public function update(UpdateTrilhaRequest $request, $id)
    {
        $trilha = Trilha::find($id);
        $trilha->update($request->all());
        $trilha->recursos()->sync($request->input('recursos', []));

        return (new TrilhaResource($trilha))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Trilha $trilha)
    {
        abort_if(Gate::denies('trilha_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $trilha->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
