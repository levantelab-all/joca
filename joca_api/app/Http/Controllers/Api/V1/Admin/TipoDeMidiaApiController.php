<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTipoDeMidiumRequest;
use App\Http\Requests\UpdateTipoDeMidiumRequest;
use App\Http\Resources\Admin\TipoDeMidiumResource;
use App\Models\TipoDeMidium;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TipoDeMidiaApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('tipo_de_midium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TipoDeMidiumResource(TipoDeMidium::with(['recursos'])->get());
    }

    public function store(StoreTipoDeMidiumRequest $request)
    {
        $tipoDeMidium = TipoDeMidium::create($request->all());
        $tipoDeMidium->recursos()->sync($request->input('recursos', []));

        return (new TipoDeMidiumResource($tipoDeMidium))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(TipoDeMidium $tipoDeMidium)
    {
        abort_if(Gate::denies('tipo_de_midium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TipoDeMidiumResource($tipoDeMidium->load(['recursos']));
    }

    public function update(UpdateTipoDeMidiumRequest $request, $id)
    {
        $tipoDeMidium = TipoDeMidium::find($id);
        $tipoDeMidium->update($request->all());
        $tipoDeMidium->recursos()->sync($request->input('recursos', []));

        return (new TipoDeMidiumResource($tipoDeMidium))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(TipoDeMidium $tipoDeMidium)
    {
        abort_if(Gate::denies('tipo_de_midium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tipoDeMidium->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
