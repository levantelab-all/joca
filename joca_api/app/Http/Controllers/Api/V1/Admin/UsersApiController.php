<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AlterPasswordRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\Admin\UserResource;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class UsersApiController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $query = new User();
        
        if ($request->has('orderBy')) {
            $query = $query->orderBy('created_at', $request->orderBy);
        }

        if ($request->has('orderBy') && $request->has('orderByColumn')) {
            $query = $query->orderBy($request->orderByColumn, $request->orderBy);
        }

        //Registro apagados
        if ($request->filled("inative")) {
            $query = $query->onlyTrashed();
        }

        $users = $query->paginate();

        return new UserResource($users->load(['roles', 'aproved_by']));
    }

    public function store(StoreUserRequest $request)
    {
        $user = User::create($request->all());
        $user->roles()->sync($request->input('roles', []));

        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(User $user)
    {
        abort_if(Gate::denies('user_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new UserResource($user->load(['roles', 'aproved_by']));
    }

    public function alterPassword(AlterPasswordRequest $request){
        $user = auth()->user();
        if(Hash::check($request->password, $user->password)){
            $user->password = bcrypt($request->new_password);
            $user->save();
            return response()->json(['msg' => 'A sua senha foi atualizada!'], Response::HTTP_ACCEPTED);
        } else {
            return response()->json(['msg' => 'A senha atual digitada não confere!'], Response::HTTP_NOT_FOUND);
        }
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->all());
        $user->roles()->sync($request->input('roles', []));

        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(User $user)
    {
        abort_if(Gate::denies('user_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
