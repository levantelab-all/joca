<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTipoDeRecursoRequest;
use App\Http\Requests\UpdateTipoDeRecursoRequest;
use App\Http\Resources\Admin\TipoDeRecursoResource;
use App\Models\TipoDeRecurso;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TipoDeRecursosApiController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('tipo_de_recurso_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tipos = new TipoDeRecurso();
        if ($request->get('not_paginate')) {
            $tipos = $tipos->get();
        }else{
            $tipos = $tipos->paginate($request->get('show', 10));
        }

        return new TipoDeRecursoResource($tipos);
    }

    public function store(StoreTipoDeRecursoRequest $request)
    {
        $tipoDeRecurso = TipoDeRecurso::create($request->all());

        return (new TipoDeRecursoResource($tipoDeRecurso))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(TipoDeRecurso $tipoDeRecurso)
    {
        abort_if(Gate::denies('tipo_de_recurso_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TipoDeRecursoResource($tipoDeRecurso);
    }

    public function update(UpdateTipoDeRecursoRequest $request, $id)
    {
        $tipoDeRecurso = TipoDeRecurso::find($id);
        $tipoDeRecurso->update($request->all());

        return (new TipoDeRecursoResource($tipoDeRecurso))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(TipoDeRecurso $tipoDeRecurso)
    {
        abort_if(Gate::denies('tipo_de_recurso_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tipoDeRecurso->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
