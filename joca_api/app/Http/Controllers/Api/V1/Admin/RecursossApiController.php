<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRecursossRequest;
use App\Http\Requests\UpdateRecursossRequest;
use App\Http\Resources\Admin\RecursossResource;
use App\Models\Recursoss;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class RecursossApiController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('recursoss_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $recursoss = new Recursoss();

        if ($request->get('not_paginate')) {
            $recursoss = $recursoss->get();
        }else{
            $recursoss = $recursoss->paginate($request->get('show', 10));
        }
        return new RecursossResource($recursoss);
    }

    public function store(StoreRecursossRequest $request)
    {
        $recursoss = Recursoss::create($request->all());

        if($recursoss){
            $recursoss->tiposRecurso()->sync($request->input('tipos', []));
            $recursoss->recursosTrilhas()->sync($request->input('trilhas', []));
            $recursoss->eixoTematicos()->sync($request->input('eixos', []));
            $imagem = $request->file('miniature');
            if($imagem){
                $recursoss->thumbnail = $this->uploadMiniature($imagem, $recursoss->uri);
                $recursoss->save();
            }
        }

        return (new RecursossResource($recursoss))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function uploadMiniature($file, $uri){
        $extensao = '.'.$file->getClientOriginalExtension();
        $upload = $file->storeAs('miniatures',  $uri.$extensao, 's3');
        if($upload){
            return $upload;
        }
    }

    public function show(Recursoss $recursoss)
    {
        abort_if(Gate::denies('recursoss_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $recursoss->append('image');
        $recursoss->load(['tiposRecurso', 'recursosTrilhas', 'eixoTematicos']);
        return new RecursossResource($recursoss);
    }

    public function update(UpdateRecursossRequest $request, $id)
    {
        $recursoss = Recursoss::find($id);
        $updated = $recursoss->update($request->all());

        if($updated){
            $recursoss->tiposRecurso()->sync($request->input('tipos', []));
            $recursoss->recursosTrilhas()->sync($request->input('trilhas', []));
            $recursoss->eixoTematicos()->sync($request->input('eixos', []));

            $imagem = $request->file('miniature');
            if($imagem){
                if(Storage::disk('s3')->exists($recursoss->thumbnail)):
                    Storage::delete($recursoss->thumbnail);
                endif;

                $recursoss->thumbnail = $this->uploadMiniature($imagem, $recursoss->uri);
                $recursoss->save();
            }
        }

        return (new RecursossResource($recursoss))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Recursoss $recursoss)
    {
        abort_if(Gate::denies('recursoss_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $deleted = $recursoss->delete();

        return response()->json(null, Response::HTTP_OK);
    }
}
