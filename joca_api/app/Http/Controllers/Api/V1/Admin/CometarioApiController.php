<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCometarioRequest;
use App\Http\Requests\UpdateCometarioRequest;
use App\Http\Resources\Admin\CometarioResource;
use App\Models\Cometario;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CometarioApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('cometario_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CometarioResource(Cometario::all());
    }

    public function store(StoreCometarioRequest $request)
    {
        $cometario = Cometario::create($request->all());

        return (new CometarioResource($cometario))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Cometario $cometario)
    {
        abort_if(Gate::denies('cometario_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CometarioResource($cometario);
    }

    public function update(UpdateCometarioRequest $request, $id)
    {
        $cometario = Cometario::find($id);
        $cometario->update($request->all());

        return (new CometarioResource($cometario))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Cometario $cometario)
    {
        abort_if(Gate::denies('cometario_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cometario->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
