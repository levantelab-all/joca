@component('mail::message')
Olá <b>{{ $name }}</b>, uma solicitação de alteração de senha foi identificada para o email {{ $email }}.
Favor, clique no botão para proceguir com a alteração.
@component('mail::button', ['url' => $url])
Alterar senha
@endcomponent
Se não foi você que fez esta solicitação, desconsidere este email.
@endcomponent