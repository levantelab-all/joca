<div id="sidebar" class="c-sidebar c-sidebar-fixed c-sidebar-lg-show">

    <div class="c-sidebar-brand d-md-down-none">
        <a class="c-sidebar-brand-full h4" href="#">
            {{ trans('panel.site_title') }}
        </a>
    </div>

    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.home") }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-tachometer-alt">

                </i>
                {{ trans('global.dashboard') }}
            </a>
        </li>
        @can('user_management_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/permissions*") ? "c-show" : "" }} {{ request()->is("admin/roles*") ? "c-show" : "" }} {{ request()->is("admin/users*") ? "c-show" : "" }} {{ request()->is("admin/seguidores*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-users c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.userManagement.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('permission_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/permissions") || request()->is("admin/permissions/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-unlock-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.permission.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('role_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/roles") || request()->is("admin/roles/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-briefcase c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.role.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('user_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.users.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/users") || request()->is("admin/users/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-user c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.user.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('seguidore_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.seguidores.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/seguidores") || request()->is("admin/seguidores/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.seguidore.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('recurso_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/recursosses*") ? "c-show" : "" }} {{ request()->is("admin/trilhas*") ? "c-show" : "" }} {{ request()->is("admin/tipo-de-recursos*") ? "c-show" : "" }} {{ request()->is("admin/tipo-de-midia*") ? "c-show" : "" }} {{ request()->is("admin/saves*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.recurso.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('recursoss_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.recursosses.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/recursosses") || request()->is("admin/recursosses/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.recursoss.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('trilha_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.trilhas.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/trilhas") || request()->is("admin/trilhas/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.trilha.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('tipo_de_recurso_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.tipo-de-recursos.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/tipo-de-recursos") || request()->is("admin/tipo-de-recursos/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.tipoDeRecurso.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('tipo_de_midium_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.tipo-de-midia.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/tipo-de-midia") || request()->is("admin/tipo-de-midia/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.tipoDeMidium.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('save_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.saves.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/saves") || request()->is("admin/saves/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.save.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('divulgaco_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/divugacos*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.divulgaco.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('divugaco_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.divugacos.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/divugacos") || request()->is("admin/divugacos/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.divugaco.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('cometario_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.cometarios.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/cometarios") || request()->is("admin/cometarios/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.cometario.title') }}
                </a>
            </li>
        @endcan
        @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
            @can('profile_password_edit')
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'c-active' : '' }}" href="{{ route('profile.password.edit') }}">
                        <i class="fa-fw fas fa-key c-sidebar-nav-icon">
                        </i>
                        {{ trans('global.change_password') }}
                    </a>
                </li>
            @endcan
        @endif
        <li class="c-sidebar-nav-item">
            <a href="#" class="c-sidebar-nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                <i class="c-sidebar-nav-icon fas fa-fw fa-sign-out-alt">

                </i>
                {{ trans('global.logout') }}
            </a>
        </li>
    </ul>

</div>