@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.divugaco.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.divugacos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.divugaco.fields.id') }}
                        </th>
                        <td>
                            {{ $divugaco->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.divugaco.fields.title') }}
                        </th>
                        <td>
                            {{ $divugaco->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.divugaco.fields.user') }}
                        </th>
                        <td>
                            {{ $divugaco->user->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.divugaco.fields.status') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $divugaco->status ? 'checked' : '' }}>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.divugacos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection