@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.tipoDeMidium.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.tipo-de-midia.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="recursos">{{ trans('cruds.tipoDeMidium.fields.recursos') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('recursos') ? 'is-invalid' : '' }}" name="recursos[]" id="recursos" multiple>
                    @foreach($recursos as $id => $recurso)
                        <option value="{{ $id }}" {{ in_array($id, old('recursos', [])) ? 'selected' : '' }}>{{ $recurso }}</option>
                    @endforeach
                </select>
                @if($errors->has('recursos'))
                    <div class="invalid-feedback">
                        {{ $errors->first('recursos') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.tipoDeMidium.fields.recursos_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection