@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.save.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.saves.update", [$save->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="user_id">{{ trans('cruds.save.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id">
                    @foreach($users as $id => $entry)
                        <option value="{{ $id }}" {{ (old('user_id') ? old('user_id') : $save->user->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.save.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="recurso_id">{{ trans('cruds.save.fields.recurso') }}</label>
                <select class="form-control select2 {{ $errors->has('recurso') ? 'is-invalid' : '' }}" name="recurso_id" id="recurso_id">
                    @foreach($recursos as $id => $entry)
                        <option value="{{ $id }}" {{ (old('recurso_id') ? old('recurso_id') : $save->recurso->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('recurso'))
                    <div class="invalid-feedback">
                        {{ $errors->first('recurso') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.save.fields.recurso_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection