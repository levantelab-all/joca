@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.recursoss.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.recursosses.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.id') }}
                        </th>
                        <td>
                            {{ $recursoss->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.accepted_terms_ad') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $recursoss->accepted_terms_ad ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.accepted_terms_offensive') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $recursoss->accepted_terms_offensive ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.accepted_terms_politics') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $recursoss->accepted_terms_politics ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.title') }}
                        </th>
                        <td>
                            {{ $recursoss->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.descricao') }}
                        </th>
                        <td>
                            {{ $recursoss->descricao }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.palavras_chave') }}
                        </th>
                        <td>
                            {{ $recursoss->palavras_chave }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.autoria') }}
                        </th>
                        <td>
                            {{ $recursoss->autoria }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.co_autoria') }}
                        </th>
                        <td>
                            {{ $recursoss->co_autoria }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.licenca') }}
                        </th>
                        <td>
                            {{ $recursoss->licenca }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.eixo_tematico') }}
                        </th>
                        <td>
                            {{ $recursoss->eixo_tematico }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.idioma') }}
                        </th>
                        <td>
                            {{ $recursoss->idioma }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.sugerido_para') }}
                        </th>
                        <td>
                            {{ $recursoss->sugerido_para }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.thumbnail') }}
                        </th>
                        <td>
                            {{ $recursoss->thumbnail }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recursoss.fields.removed_by') }}
                        </th>
                        <td>
                            {{ $recursoss->removed_by }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.recursosses.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#recursos_trilhas" role="tab" data-toggle="tab">
                {{ trans('cruds.trilha.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="recursos_trilhas">
            @includeIf('admin.recursosses.relationships.recursosTrilhas', ['trilhas' => $recursoss->recursosTrilhas])
        </div>
    </div>
</div>

@endsection