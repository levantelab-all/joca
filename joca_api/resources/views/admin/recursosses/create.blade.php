@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.recursoss.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.recursosses.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <div class="form-check {{ $errors->has('accepted_terms_ad') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="accepted_terms_ad" value="0">
                    <input class="form-check-input" type="checkbox" name="accepted_terms_ad" id="accepted_terms_ad" value="1" {{ old('accepted_terms_ad', 0) == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="accepted_terms_ad">{{ trans('cruds.recursoss.fields.accepted_terms_ad') }}</label>
                </div>
                @if($errors->has('accepted_terms_ad'))
                    <div class="invalid-feedback">
                        {{ $errors->first('accepted_terms_ad') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.accepted_terms_ad_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('accepted_terms_offensive') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="accepted_terms_offensive" value="0">
                    <input class="form-check-input" type="checkbox" name="accepted_terms_offensive" id="accepted_terms_offensive" value="1" {{ old('accepted_terms_offensive', 0) == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="accepted_terms_offensive">{{ trans('cruds.recursoss.fields.accepted_terms_offensive') }}</label>
                </div>
                @if($errors->has('accepted_terms_offensive'))
                    <div class="invalid-feedback">
                        {{ $errors->first('accepted_terms_offensive') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.accepted_terms_offensive_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('accepted_terms_politics') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="accepted_terms_politics" value="0">
                    <input class="form-check-input" type="checkbox" name="accepted_terms_politics" id="accepted_terms_politics" value="1" {{ old('accepted_terms_politics', 0) == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="accepted_terms_politics">{{ trans('cruds.recursoss.fields.accepted_terms_politics') }}</label>
                </div>
                @if($errors->has('accepted_terms_politics'))
                    <div class="invalid-feedback">
                        {{ $errors->first('accepted_terms_politics') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.accepted_terms_politics_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="title">{{ trans('cruds.recursoss.fields.title') }}</label>
                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', '') }}" required>
                @if($errors->has('title'))
                    <div class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.title_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="descricao">{{ trans('cruds.recursoss.fields.descricao') }}</label>
                <input class="form-control {{ $errors->has('descricao') ? 'is-invalid' : '' }}" type="text" name="descricao" id="descricao" value="{{ old('descricao', '') }}" required>
                @if($errors->has('descricao'))
                    <div class="invalid-feedback">
                        {{ $errors->first('descricao') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.descricao_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="palavras_chave">{{ trans('cruds.recursoss.fields.palavras_chave') }}</label>
                <input class="form-control {{ $errors->has('palavras_chave') ? 'is-invalid' : '' }}" type="text" name="palavras_chave" id="palavras_chave" value="{{ old('palavras_chave', '') }}">
                @if($errors->has('palavras_chave'))
                    <div class="invalid-feedback">
                        {{ $errors->first('palavras_chave') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.palavras_chave_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="autoria">{{ trans('cruds.recursoss.fields.autoria') }}</label>
                <input class="form-control {{ $errors->has('autoria') ? 'is-invalid' : '' }}" type="text" name="autoria" id="autoria" value="{{ old('autoria', '') }}">
                @if($errors->has('autoria'))
                    <div class="invalid-feedback">
                        {{ $errors->first('autoria') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.autoria_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="co_autoria">{{ trans('cruds.recursoss.fields.co_autoria') }}</label>
                <input class="form-control {{ $errors->has('co_autoria') ? 'is-invalid' : '' }}" type="text" name="co_autoria" id="co_autoria" value="{{ old('co_autoria', '') }}">
                @if($errors->has('co_autoria'))
                    <div class="invalid-feedback">
                        {{ $errors->first('co_autoria') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.co_autoria_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="licenca">{{ trans('cruds.recursoss.fields.licenca') }}</label>
                <input class="form-control {{ $errors->has('licenca') ? 'is-invalid' : '' }}" type="text" name="licenca" id="licenca" value="{{ old('licenca', '') }}" required>
                @if($errors->has('licenca'))
                    <div class="invalid-feedback">
                        {{ $errors->first('licenca') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.licenca_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="eixo_tematico">{{ trans('cruds.recursoss.fields.eixo_tematico') }}</label>
                <input class="form-control {{ $errors->has('eixo_tematico') ? 'is-invalid' : '' }}" type="text" name="eixo_tematico" id="eixo_tematico" value="{{ old('eixo_tematico', '') }}">
                @if($errors->has('eixo_tematico'))
                    <div class="invalid-feedback">
                        {{ $errors->first('eixo_tematico') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.eixo_tematico_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="idioma">{{ trans('cruds.recursoss.fields.idioma') }}</label>
                <input class="form-control {{ $errors->has('idioma') ? 'is-invalid' : '' }}" type="text" name="idioma" id="idioma" value="{{ old('idioma', '') }}">
                @if($errors->has('idioma'))
                    <div class="invalid-feedback">
                        {{ $errors->first('idioma') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.idioma_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="sugerido_para">{{ trans('cruds.recursoss.fields.sugerido_para') }}</label>
                <input class="form-control {{ $errors->has('sugerido_para') ? 'is-invalid' : '' }}" type="text" name="sugerido_para" id="sugerido_para" value="{{ old('sugerido_para', '') }}">
                @if($errors->has('sugerido_para'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sugerido_para') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.sugerido_para_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="thumbnail">{{ trans('cruds.recursoss.fields.thumbnail') }}</label>
                <input class="form-control {{ $errors->has('thumbnail') ? 'is-invalid' : '' }}" type="text" name="thumbnail" id="thumbnail" value="{{ old('thumbnail', '') }}">
                @if($errors->has('thumbnail'))
                    <div class="invalid-feedback">
                        {{ $errors->first('thumbnail') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.thumbnail_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="removed_by">{{ trans('cruds.recursoss.fields.removed_by') }}</label>
                <input class="form-control {{ $errors->has('removed_by') ? 'is-invalid' : '' }}" type="text" name="removed_by" id="removed_by" value="{{ old('removed_by', '') }}">
                @if($errors->has('removed_by'))
                    <div class="invalid-feedback">
                        {{ $errors->first('removed_by') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recursoss.fields.removed_by_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection