@extends('layouts.admin')
@section('content')
@can('recursoss_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.recursosses.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.recursoss.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.recursoss.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Recursoss">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.accepted_terms_ad') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.accepted_terms_offensive') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.accepted_terms_politics') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.title') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.descricao') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.palavras_chave') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.autoria') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.co_autoria') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.licenca') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.eixo_tematico') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.idioma') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.sugerido_para') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.thumbnail') }}
                        </th>
                        <th>
                            {{ trans('cruds.recursoss.fields.removed_by') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($recursosses as $key => $recursoss)
                        <tr data-entry-id="{{ $recursoss->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $recursoss->id ?? '' }}
                            </td>
                            <td>
                                <span style="display:none">{{ $recursoss->accepted_terms_ad ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $recursoss->accepted_terms_ad ? 'checked' : '' }}>
                            </td>
                            <td>
                                <span style="display:none">{{ $recursoss->accepted_terms_offensive ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $recursoss->accepted_terms_offensive ? 'checked' : '' }}>
                            </td>
                            <td>
                                <span style="display:none">{{ $recursoss->accepted_terms_politics ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $recursoss->accepted_terms_politics ? 'checked' : '' }}>
                            </td>
                            <td>
                                {{ $recursoss->title ?? '' }}
                            </td>
                            <td>
                                {{ $recursoss->descricao ?? '' }}
                            </td>
                            <td>
                                {{ $recursoss->palavras_chave ?? '' }}
                            </td>
                            <td>
                                {{ $recursoss->autoria ?? '' }}
                            </td>
                            <td>
                                {{ $recursoss->co_autoria ?? '' }}
                            </td>
                            <td>
                                {{ $recursoss->licenca ?? '' }}
                            </td>
                            <td>
                                {{ $recursoss->eixo_tematico ?? '' }}
                            </td>
                            <td>
                                {{ $recursoss->idioma ?? '' }}
                            </td>
                            <td>
                                {{ $recursoss->sugerido_para ?? '' }}
                            </td>
                            <td>
                                {{ $recursoss->thumbnail ?? '' }}
                            </td>
                            <td>
                                {{ $recursoss->removed_by ?? '' }}
                            </td>
                            <td>
                                @can('recursoss_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.recursosses.show', $recursoss->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('recursoss_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.recursosses.edit', $recursoss->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('recursoss_delete')
                                    <form action="{{ route('admin.recursosses.destroy', $recursoss->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('recursoss_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.recursosses.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Recursoss:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection