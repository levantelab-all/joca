@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.seguidore.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.seguidores.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="follow_id">{{ trans('cruds.seguidore.fields.follow') }}</label>
                <select class="form-control select2 {{ $errors->has('follow') ? 'is-invalid' : '' }}" name="follow_id" id="follow_id">
                    @foreach($follows as $id => $entry)
                        <option value="{{ $id }}" {{ old('follow_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('follow'))
                    <div class="invalid-feedback">
                        {{ $errors->first('follow') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.seguidore.fields.follow_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="user_id">{{ trans('cruds.seguidore.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id">
                    @foreach($users as $id => $entry)
                        <option value="{{ $id }}" {{ old('user_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.seguidore.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection