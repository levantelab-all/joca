@extends('layouts.admin')
@section('content')
@can('trilha_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.trilhas.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.trilha.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.trilha.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Trilha">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.trilha.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.trilha.fields.titulo') }}
                        </th>
                        <th>
                            {{ trans('cruds.trilha.fields.status') }}
                        </th>
                        <th>
                            {{ trans('cruds.trilha.fields.recursos') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($trilhas as $key => $trilha)
                        <tr data-entry-id="{{ $trilha->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $trilha->id ?? '' }}
                            </td>
                            <td>
                                {{ $trilha->titulo ?? '' }}
                            </td>
                            <td>
                                <span style="display:none">{{ $trilha->status ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $trilha->status ? 'checked' : '' }}>
                            </td>
                            <td>
                                @foreach($trilha->recursos as $key => $item)
                                    <span class="badge badge-info">{{ $item->title }}</span>
                                @endforeach
                            </td>
                            <td>
                                @can('trilha_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.trilhas.show', $trilha->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('trilha_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.trilhas.edit', $trilha->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('trilha_delete')
                                    <form action="{{ route('admin.trilhas.destroy', $trilha->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('trilha_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.trilhas.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Trilha:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection