@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.trilha.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.trilhas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.trilha.fields.id') }}
                        </th>
                        <td>
                            {{ $trilha->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trilha.fields.titulo') }}
                        </th>
                        <td>
                            {{ $trilha->titulo }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trilha.fields.status') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $trilha->status ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trilha.fields.recursos') }}
                        </th>
                        <td>
                            @foreach($trilha->recursos as $key => $recursos)
                                <span class="label label-info">{{ $recursos->title }}</span>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.trilhas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection