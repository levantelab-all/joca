@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.trilha.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.trilhas.update", [$trilha->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="titulo">{{ trans('cruds.trilha.fields.titulo') }}</label>
                <input class="form-control {{ $errors->has('titulo') ? 'is-invalid' : '' }}" type="text" name="titulo" id="titulo" value="{{ old('titulo', $trilha->titulo) }}" required>
                @if($errors->has('titulo'))
                    <div class="invalid-feedback">
                        {{ $errors->first('titulo') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.trilha.fields.titulo_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('status') ? 'is-invalid' : '' }}">
                    <input class="form-check-input" type="checkbox" name="status" id="status" value="1" {{ $trilha->status || old('status', 0) === 1 ? 'checked' : '' }} required>
                    <label class="required form-check-label" for="status">{{ trans('cruds.trilha.fields.status') }}</label>
                </div>
                @if($errors->has('status'))
                    <div class="invalid-feedback">
                        {{ $errors->first('status') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.trilha.fields.status_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="recursos">{{ trans('cruds.trilha.fields.recursos') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('recursos') ? 'is-invalid' : '' }}" name="recursos[]" id="recursos" multiple>
                    @foreach($recursos as $id => $recurso)
                        <option value="{{ $id }}" {{ (in_array($id, old('recursos', [])) || $trilha->recursos->contains($id)) ? 'selected' : '' }}>{{ $recurso }}</option>
                    @endforeach
                </select>
                @if($errors->has('recursos'))
                    <div class="invalid-feedback">
                        {{ $errors->first('recursos') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.trilha.fields.recursos_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection