@extends('layouts.admin')
@section('content')
@can('user_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.users.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.user.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.user.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.email') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.email_verified_at') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.sobrenome') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.nome_social') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.genero') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.data_nascimento') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.user_type') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.permissao') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.matricula') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.curso') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.cep') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.estado') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.municipio') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.area_atuacao') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.instituicao_de_atuacao') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.como_conheceu') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.termos_accept') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.accept_emails') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.aproved_by') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.email') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.numero_ceap') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $user)
                        <tr data-entry-id="{{ $user->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $user->id ?? '' }}
                            </td>
                            <td>
                                {{ $user->name ?? '' }}
                            </td>
                            <td>
                                {{ $user->email ?? '' }}
                            </td>
                            <td>
                                {{ $user->email_verified_at ?? '' }}
                            </td>
                            <td>
                                @foreach($user->roles as $key => $item)
                                    <span class="badge badge-info">{{ $item->title }}</span>
                                @endforeach
                            </td>
                            <td>
                                {{ $user->sobrenome ?? '' }}
                            </td>
                            <td>
                                {{ $user->nome_social ?? '' }}
                            </td>
                            <td>
                                {{ $user->genero ?? '' }}
                            </td>
                            <td>
                                {{ $user->data_nascimento ?? '' }}
                            </td>
                            <td>
                                {{ $user->user_type ?? '' }}
                            </td>
                            <td>
                                {{ $user->permissao ?? '' }}
                            </td>
                            <td>
                                {{ $user->matricula ?? '' }}
                            </td>
                            <td>
                                {{ $user->curso ?? '' }}
                            </td>
                            <td>
                                {{ $user->cep ?? '' }}
                            </td>
                            <td>
                                {{ $user->estado ?? '' }}
                            </td>
                            <td>
                                {{ $user->municipio ?? '' }}
                            </td>
                            <td>
                                {{ $user->area_atuacao ?? '' }}
                            </td>
                            <td>
                                {{ $user->instituicao_de_atuacao ?? '' }}
                            </td>
                            <td>
                                {{ $user->como_conheceu ?? '' }}
                            </td>
                            <td>
                                <span style="display:none">{{ $user->termos_accept ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $user->termos_accept ? 'checked' : '' }}>
                            </td>
                            <td>
                                <span style="display:none">{{ $user->accept_emails ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $user->accept_emails ? 'checked' : '' }}>
                            </td>
                            <td>
                                {{ $user->aproved_by->name ?? '' }}
                            </td>
                            <td>
                                {{ $user->aproved_by->email ?? '' }}
                            </td>
                            <td>
                                {{ $user->numero_ceap ?? '' }}
                            </td>
                            <td>
                                @can('user_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.users.show', $user->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('user_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.users.edit', $user->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('user_delete')
                                    <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('user_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.users.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection