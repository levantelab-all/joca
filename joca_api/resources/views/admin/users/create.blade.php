@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.users.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.user.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="email">{{ trans('cruds.user.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" id="email" value="{{ old('email') }}" required>
                @if($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="password">{{ trans('cruds.user.fields.password') }}</label>
                <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" id="password" required>
                @if($errors->has('password'))
                    <div class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.password_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="roles">{{ trans('cruds.user.fields.roles') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('roles') ? 'is-invalid' : '' }}" name="roles[]" id="roles" multiple required>
                    @foreach($roles as $id => $role)
                        <option value="{{ $id }}" {{ in_array($id, old('roles', [])) ? 'selected' : '' }}>{{ $role }}</option>
                    @endforeach
                </select>
                @if($errors->has('roles'))
                    <div class="invalid-feedback">
                        {{ $errors->first('roles') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.roles_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="sobrenome">{{ trans('cruds.user.fields.sobrenome') }}</label>
                <input class="form-control {{ $errors->has('sobrenome') ? 'is-invalid' : '' }}" type="text" name="sobrenome" id="sobrenome" value="{{ old('sobrenome', '') }}" required>
                @if($errors->has('sobrenome'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sobrenome') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.sobrenome_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="nome_social">{{ trans('cruds.user.fields.nome_social') }}</label>
                <input class="form-control {{ $errors->has('nome_social') ? 'is-invalid' : '' }}" type="text" name="nome_social" id="nome_social" value="{{ old('nome_social', '') }}">
                @if($errors->has('nome_social'))
                    <div class="invalid-feedback">
                        {{ $errors->first('nome_social') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.nome_social_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="genero">{{ trans('cruds.user.fields.genero') }}</label>
                <input class="form-control {{ $errors->has('genero') ? 'is-invalid' : '' }}" type="text" name="genero" id="genero" value="{{ old('genero', '') }}" required>
                @if($errors->has('genero'))
                    <div class="invalid-feedback">
                        {{ $errors->first('genero') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.genero_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="data_nascimento">{{ trans('cruds.user.fields.data_nascimento') }}</label>
                <input class="form-control datetime {{ $errors->has('data_nascimento') ? 'is-invalid' : '' }}" type="text" name="data_nascimento" id="data_nascimento" value="{{ old('data_nascimento') }}" required>
                @if($errors->has('data_nascimento'))
                    <div class="invalid-feedback">
                        {{ $errors->first('data_nascimento') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.data_nascimento_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="user_type">{{ trans('cruds.user.fields.user_type') }}</label>
                <input class="form-control {{ $errors->has('user_type') ? 'is-invalid' : '' }}" type="number" name="user_type" id="user_type" value="{{ old('user_type', '') }}" step="1">
                @if($errors->has('user_type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user_type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.user_type_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="permissao">{{ trans('cruds.user.fields.permissao') }}</label>
                <input class="form-control {{ $errors->has('permissao') ? 'is-invalid' : '' }}" type="number" name="permissao" id="permissao" value="{{ old('permissao', '') }}" step="1" required>
                @if($errors->has('permissao'))
                    <div class="invalid-feedback">
                        {{ $errors->first('permissao') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.permissao_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="matricula">{{ trans('cruds.user.fields.matricula') }}</label>
                <input class="form-control {{ $errors->has('matricula') ? 'is-invalid' : '' }}" type="text" name="matricula" id="matricula" value="{{ old('matricula', '') }}">
                @if($errors->has('matricula'))
                    <div class="invalid-feedback">
                        {{ $errors->first('matricula') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.matricula_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="curso">{{ trans('cruds.user.fields.curso') }}</label>
                <input class="form-control {{ $errors->has('curso') ? 'is-invalid' : '' }}" type="text" name="curso" id="curso" value="{{ old('curso', '') }}" required>
                @if($errors->has('curso'))
                    <div class="invalid-feedback">
                        {{ $errors->first('curso') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.curso_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="cep">{{ trans('cruds.user.fields.cep') }}</label>
                <input class="form-control {{ $errors->has('cep') ? 'is-invalid' : '' }}" type="text" name="cep" id="cep" value="{{ old('cep', '') }}" required>
                @if($errors->has('cep'))
                    <div class="invalid-feedback">
                        {{ $errors->first('cep') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.cep_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="estado">{{ trans('cruds.user.fields.estado') }}</label>
                <input class="form-control {{ $errors->has('estado') ? 'is-invalid' : '' }}" type="text" name="estado" id="estado" value="{{ old('estado', '') }}" required>
                @if($errors->has('estado'))
                    <div class="invalid-feedback">
                        {{ $errors->first('estado') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.estado_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="municipio">{{ trans('cruds.user.fields.municipio') }}</label>
                <input class="form-control {{ $errors->has('municipio') ? 'is-invalid' : '' }}" type="text" name="municipio" id="municipio" value="{{ old('municipio', '') }}" required>
                @if($errors->has('municipio'))
                    <div class="invalid-feedback">
                        {{ $errors->first('municipio') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.municipio_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="area_atuacao">{{ trans('cruds.user.fields.area_atuacao') }}</label>
                <input class="form-control {{ $errors->has('area_atuacao') ? 'is-invalid' : '' }}" type="text" name="area_atuacao" id="area_atuacao" value="{{ old('area_atuacao', '') }}">
                @if($errors->has('area_atuacao'))
                    <div class="invalid-feedback">
                        {{ $errors->first('area_atuacao') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.area_atuacao_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="instituicao_de_atuacao">{{ trans('cruds.user.fields.instituicao_de_atuacao') }}</label>
                <input class="form-control {{ $errors->has('instituicao_de_atuacao') ? 'is-invalid' : '' }}" type="text" name="instituicao_de_atuacao" id="instituicao_de_atuacao" value="{{ old('instituicao_de_atuacao', '') }}">
                @if($errors->has('instituicao_de_atuacao'))
                    <div class="invalid-feedback">
                        {{ $errors->first('instituicao_de_atuacao') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.instituicao_de_atuacao_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="como_conheceu">{{ trans('cruds.user.fields.como_conheceu') }}</label>
                <input class="form-control {{ $errors->has('como_conheceu') ? 'is-invalid' : '' }}" type="text" name="como_conheceu" id="como_conheceu" value="{{ old('como_conheceu', '') }}">
                @if($errors->has('como_conheceu'))
                    <div class="invalid-feedback">
                        {{ $errors->first('como_conheceu') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.como_conheceu_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('termos_accept') ? 'is-invalid' : '' }}">
                    <input class="form-check-input" type="checkbox" name="termos_accept" id="termos_accept" value="1" required {{ old('termos_accept', 0) == 1 ? 'checked' : '' }}>
                    <label class="required form-check-label" for="termos_accept">{{ trans('cruds.user.fields.termos_accept') }}</label>
                </div>
                @if($errors->has('termos_accept'))
                    <div class="invalid-feedback">
                        {{ $errors->first('termos_accept') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.termos_accept_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('accept_emails') ? 'is-invalid' : '' }}">
                    <input class="form-check-input" type="checkbox" name="accept_emails" id="accept_emails" value="1" required {{ old('accept_emails', 0) == 1 || old('accept_emails') === null ? 'checked' : '' }}>
                    <label class="required form-check-label" for="accept_emails">{{ trans('cruds.user.fields.accept_emails') }}</label>
                </div>
                @if($errors->has('accept_emails'))
                    <div class="invalid-feedback">
                        {{ $errors->first('accept_emails') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.accept_emails_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="aproved_by_id">{{ trans('cruds.user.fields.aproved_by') }}</label>
                <select class="form-control select2 {{ $errors->has('aproved_by') ? 'is-invalid' : '' }}" name="aproved_by_id" id="aproved_by_id">
                    @foreach($aproved_bies as $id => $entry)
                        <option value="{{ $id }}" {{ old('aproved_by_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('aproved_by'))
                    <div class="invalid-feedback">
                        {{ $errors->first('aproved_by') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.aproved_by_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="numero_ceap">{{ trans('cruds.user.fields.numero_ceap') }}</label>
                <input class="form-control {{ $errors->has('numero_ceap') ? 'is-invalid' : '' }}" type="text" name="numero_ceap" id="numero_ceap" value="{{ old('numero_ceap', '') }}">
                @if($errors->has('numero_ceap'))
                    <div class="invalid-feedback">
                        {{ $errors->first('numero_ceap') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.numero_ceap_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection