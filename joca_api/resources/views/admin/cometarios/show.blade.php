@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.cometario.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.cometarios.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.cometario.fields.id') }}
                        </th>
                        <td>
                            {{ $cometario->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cometario.fields.comentario') }}
                        </th>
                        <td>
                            {{ $cometario->comentario }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.cometario.fields.like') }}
                        </th>
                        <td>
                            {{ $cometario->like }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.cometarios.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection