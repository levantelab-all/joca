@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.cometario.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.cometarios.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="comentario">{{ trans('cruds.cometario.fields.comentario') }}</label>
                <input class="form-control {{ $errors->has('comentario') ? 'is-invalid' : '' }}" type="text" name="comentario" id="comentario" value="{{ old('comentario', '') }}">
                @if($errors->has('comentario'))
                    <div class="invalid-feedback">
                        {{ $errors->first('comentario') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.cometario.fields.comentario_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="like">{{ trans('cruds.cometario.fields.like') }}</label>
                <input class="form-control {{ $errors->has('like') ? 'is-invalid' : '' }}" type="number" name="like" id="like" value="{{ old('like', '0') }}" step="1">
                @if($errors->has('like'))
                    <div class="invalid-feedback">
                        {{ $errors->first('like') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.cometario.fields.like_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection